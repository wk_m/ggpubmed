---
title: "GGPubmed Howto"
author: "Wolfgang K Matzek"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{GGPubmed Howto}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---
```{r startup}
knitr::opts_chunk$set(dev = 'svg', fig.width = 7)
```

## Overview
The aim of this package is to help researchers in performing basic tasks of literature search. ggpubmed is a collection of tools for querying pubmed and other electronic search tools of the NBCI by using the RISmed package.

## Retrieving data
By using `getItemData()`, a dataset will be retrieved for a given search term. The syntax and semantics of the search term is equivalent to the pubmed.org web interface. `getItemData()` will accept the following options: `mindate` for defining the earliest year of result, `maxdate` for defining the latest year, defaulting to the present year. Further, `retmax`, which limits the number of results, defaulting to 1000.

```{r init_ggpubmed}
library(ggpubmed)
ds <- getItemData("endovascular repair of inflammatory aortic aneurysm")
```

`ds` will hold a list of the following data:

1. first_author, last_author, title, journal, year, abstract
2. a list of all authors with their abbreviated first name
3. the search term

## Plotting bar graphs
First, we would like to plot a bar graph of publications per year.

```{r get_years}
getYears(ds)
```

Next, we are interested in the first authors for a given topic. The function `getFirstAuthors()` does accept one option, max_results for limiting the ranks of authors. It defaults to 25.

```{r first_authors}
getFirstAuthors(ds)
```

It is also possible to plot a bar plot of all authors. As with `getFirstAuthors()`, this function also accapts the option max_results.

```{r all_authors}
getAllAuthors(ds)
```

Finally, `getJournals()` will plot a graph of relevant journals.

```{r get_journals}
getJournals(ds)
```

## Plotting maps
`getJYMap()` plots a choropleth diagram for frequency by journals and publication years. This is only useful for a limited number of authors and relevant journals.

```{r getJYMap}
getJYMap(ds)
```

## Creating a wordcloud
There is another nice function for displaying terms found in the abstract by their frequency using a wordcloud. It is useful to derive other search terms or topics related to the given search term.

```{r wc_abstract, fig.height=5, fig.width=5}
wcAbstractByKey(ds)
```

When using `wcAuthByKey()`, a wordcloud is plotted from a list of all authors. This is useful for getting relevant authors and co-authors for a given topic, refer to the bar plot functions `getFirstAuthor()` and `getAllAuthors()`.

```{r wc_authors, fig.height=5, fig.width=5}
wcAuthByKey(ds)
```
