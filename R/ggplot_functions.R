
# Literature research
# - What's published about a topic
# - When did most abstracts appear - done
# - What's hot now
# - Who's written most? - done
# - What are the relevant journals - done
# - Who's authors works are most cited and in which journals
# - What are the choosen keywords / MeSH Terms

## package name could be ggpubmed


#' Get the most cited authors for a given search term and display a bar plot.
#'
#' @param item_data A list object containing the pubmed query result from getItemData()
#'
#' @return a bar plot
#' @export
#' @import ggplot2 dplyr
#'
#' @examples getItemData("your search term") %>% getMostCited()
getMostCited <- function(item_data, max_results = 25) {
  item_data[[1]] %>%
    mutate(cited = pmid %>% getCites()) %>%
    arrange(desc(cited)) %>%
    head(n = max_results) %>%
    ggplot(aes(x = reorder(paste(first_author, paste("(", last_author, ")", sep = ""), journal, year), cited), y = cited, fill = cited)) +
    geom_bar(stat = "identity") +
    coord_flip() +
    geom_text(aes(label = cited), hjust = 1.5, colour = "white", size = 3) +
    theme(legend.position = "none", text = element_text(size = 13)) +
    scale_fill_gradient(low = "dark red", high = "red") +
    xlab("publication") + ylab("number of cites") +
    ggtitle(paste("most cited publications:", item_data[3] %>% unlist()))

  # old function used:
  #    cited <- item_data[1] %>%
  #    sapply(function(x) paste(x$pmid)) %>%
  #    getCites()
  #
  #      data.frame(item_data[1], cited) %>%
  #      ...

}

#' Get all authors of given publications despite their position and display a bar plot of frequencies
#'
#' @param item_data  A list object containing the pubmed query result from getItemData()
#'
#' @return a bar plot
#' @export
#' @import ggplot2 dplyr tibble
#'
#' @examples getItemData("your search term") %>% getAllAuthors()
getAllAuthors <- function(item_data, max_results = 25) {
  as.tibble(item_data[2]) %>%
    count(all_authors) %>%
    arrange(desc(n)) %>%
    head(n = max_results) %>%
    ggplot(aes(x = reorder(all_authors, n), y = n, fill = n)) +
    geom_bar(stat = "identity") +
    coord_flip() +
    geom_text(aes(label = n), hjust = 1.5, colour = "white", size = 3) +
    theme(legend.position = "none", text = element_text(size = 13)) +
    xlab("year") + ylab("n") +
    ggtitle(paste("publications per author:", item_data[3] %>% unlist()))
}
#     scale_fill_gradient(low="dark blue", high="light blue") +

#' Get the first author of given publications and display a bar plot of frequencies
#'
#' @param item_data  A list object containing the pubmed query result from getItemData()
#'
#' @return a bar plot
#' @export
#' @import ggplot2 dplyr
#'
#' @examples getItemData("your search term") %>% getFirstAuthors()
getFirstAuthors <- function(item_data, max_results = 25) {
  item_data[[1]] %>%
    count(first_author) %>%
    arrange(desc(n)) %>%
    head(n = max_results) %>%
    ggplot(aes(x = reorder(first_author, n), y = n, fill = n)) +
    geom_bar(stat = "identity") +
    coord_flip() +
    geom_text(aes(label = n), hjust = 1.5, colour = "white", size = 3) +
    theme(legend.position = "none", text = element_text(size = 13)) +
    scale_fill_gradient(low = "dark red", high = "red") +
    xlab("first author") + ylab("n") +
    ggtitle(paste("publications per first author:", item_data[3] %>% unlist()))
}

# rank() would return a rank instead of an absolute counts
# for filling using continous scales (freq): scale_fill_gradient(low="blue", high="red")
# for filling using dicrete scale (quthor_list): scale_fill_manual(values = colorRampPalette(brewer.pal(11,'Spectral'))(max_results)) +


# optional: filter(freq > 2) %>%
# scale_fill_brewer(palette = "Spectral") +

#' Get the last author of given publications and display a bar plot of frequencies
#'
#' @param item_data  A list object containing the pubmed query result from getItemData()
#' @param max_results
#'
#' @return a bar plot
#' @export
#' @import ggplot2 dplyr
#'
#' @examples getItemData("your search term") %>% getLastAuthors()
getLastAuthors <- function(item_data, max_results = 25) {
  item_data[[1]] %>%
    count(last_author) %>%
    arrange(desc(n)) %>%
    head(n = max_results) %>%
    ggplot(aes(x = reorder(last_author, n), y = n, fill = n)) +
    geom_bar(stat = "identity") +
    coord_flip() +
    geom_text(aes(label = n), hjust = 1.5, colour = "white", size = 3) +
    theme(legend.position = "none", text = element_text(size = 13)) +
    scale_fill_gradient(low = "dark red", high = "red") +
    xlab("last author") + ylab("n") +
    ggtitle(paste("publications per last author:", item_data[3] %>% unlist()))
}

#' Get the year of given publications and display a bar plot of frequencies
#'
#' @param item_data  A list object containing the pubmed query result from getItemData()
#'
#' @return a bar plot
#' @export
#' @import ggplot2 dplyr
#'
#' @examples getItemData("your search term") %>% getYears()
# This was the old code calculating a row count first and put it to ggplot2. The new function uses ggplot's functions.
# getYears <- function(item_data) {
#   data.frame(item_data[1])["year"] %>%
#     count(year) %>%
#     ggplot(aes(x = year, y = n, fill = n)) +
#     geom_bar(stat = "identity") +
#     coord_flip() +
#     geom_text(aes(label = n), hjust = 1.5, colour = "white", size = 3) +
#     theme(legend.position = "none", text = element_text(size = 13)) +
#     xlab("year") + ylab(paste("n =", data.frame(item_data[1])["pmid"] %>% count)) +
#     ggtitle(paste("publications per year:", item_data[3] %>% unlist()))
# }
getYears <- function(item_data) {
  item_data[[1]] %>%
    ggplot(aes(x = year, fill = ..count..)) +
    geom_bar() +
    coord_flip() +
    geom_text(stat = "count", aes(label = ..count..), hjust = 1.5, colour = "white", size = 3) +
    theme(legend.position = "none", text = element_text(size = 13)) +
    xlab("year") + ylab(paste("n =", data.frame(item_data[1])["pmid"] %>% count)) +
    ggtitle(paste("publications per year:", item_data[3] %>% unlist()))
}

#' Get the journal title of given publications and display a bar plot of frequencies
#'
#' @param item_data  A list object containing the pubmed query result from getItemData()
#'
#' @return a bar plot
#' @export
#' @import ggplot2 dplyr
#'
#' @examples getItemData("your search term") %>% getJournals()
getJournals <- function(item_data, max_results = 25) {
  item_data[[1]] %>%
    count(journal) %>%
    arrange(desc(n)) %>%
    head(n = max_results) %>%
    ggplot(aes(x = reorder(journal, n), y = n, fill = n)) +
    geom_bar(stat = "identity") +
    coord_flip() +
    geom_text(aes(label = n), hjust = 1.5, colour = "white", size = 3) +
    theme(legend.position = "none", text = element_text(size = 13)) +
    scale_fill_gradient(low = "dark red", high = "red") +
    xlab("journal") + ylab("n") +
    ggtitle(paste("publications per journal:", item_data[3] %>% unlist()))
}

#' display a map plot of frequencies per journal and year
#'
#' @param item_data  A list object containing the pubmed query result from getItemData()
#'
#' @return a bar plot
#' @export
#' @import ggplot2 dplyr
#'
#' @examples getItemData("your search term") %>% getJYMap()
getJYMap <- function(item_data) {
  item_data[[1]] %>%
    count(year, journal) %>%
    ggplot(aes(x = year, y = journal, fill = n)) +
    geom_tile() +
    scale_fill_distiller(palette = "RdYlBu") +
    ggtitle(paste("publications per journal and year:", item_data[3] %>% unlist()))
}

# Title
# Mesh() # return MeSH terms
# Cited() # returns number of citations
# Country()
# PMID() returns PMIDs of a Medline object

# also see the rentrez package
# https://cran.r-project.org/web/packages/rentrez/vignettes/rentrez_tutorial.html
# and the fulltext package by ropensci - allows to retrieve journal fulltext
# https://ropensci.org/tutorials/fulltext_tutorial.html
