#' Get pubmed items as Medline object.
#'
#' @param search_term the term to search for
#' @param type type of search; defaults to 'esearch'
#' @param db type of database to search; defaults to 'pubmed'
#' @param mindate the minimum year to look for results; defaults to 1900
#' @param maxdate the maximum year to look for results; defaults to today
#' @param retmax the maximum count of results; defaults to 1000
#'
#' @return returns a Medline / RISmed object
#' @export
#' @import RISmed dplyr lubridate
#'
getItems <- function(search_term, type = 'esearch', db = 'pubmed', mindate = 1900, maxdate = year(now()), retmax = 1000) {
  EUtilsSummary(search_term, type = type, db = db, mindate = mindate, maxdate = maxdate, retmax = retmax, encoding = "UTF-8") %>%
    # QueryId() %>% removed
    EUtilsGet()
}

#' create a bar plot of first authors ordered by rank of publication count
#'
#' @param item the result of getItemData()
#'
#' @return a bar plot
#' @export
#' @import dplyr
#'
firstAuthor <- function(item) {
  Author(item) %>%
    sapply(function(x) paste(x$LastName[1], substr(x$ForeName[1], 1,1)))
}

#' Returns a list of keywords of a list of publications.
#'
#' @param item the result of getItemData()
#'
#' @return a list of keywords
#' @export
#' @import RISmed dplyr stringr
#'
keyWords <- function(item) {
  RISmed::Mesh(item) %>%
    sapply(function(x) if (!anyNA(x)) paste(x$Type, x$Heading)) %>%
    sapply(function(x) paste(str_replace_all(str_c(x, collapse = ""), c("Descriptor" = ";", "Qualifier" = ",")))) %>%
    str_sub(3)
  # ugly function, after all...
}

#' Get pubmed items as a list
#' @param search_term the term to search for
#' @param type type of search; defaults to 'esearch'
#' @param db type of database to search; defaults to 'pubmed'
#' @param mindate the minimum year to look for results; defaults to 1900
#' @param maxdate the maximum year to look for results; defaults to today
#' @param retmax the maximum count of results; defaults to 1000
#'
#' @return [1] a list of first_author, last_author, title, journal, year, abstract [2] a list of all authors [3] the search term, also encapsulated as a list
#'
#' @export
#' @import dplyr lubridate
#'
#' @examples res = getItemData("Your search term")
getItemData <- function(search_term, type = 'esearch', db = 'pubmed', mindate = 1900, maxdate = year(now()), retmax = 1000) {
  da <- getItems(search_term = search_term, type = type, db = db, mindate = mindate, maxdate = maxdate, retmax = retmax)
  list(tibble(pmid    = PMID(da),
                  first_author  = Author(da) %>% sapply(function(x) paste(x$LastName[1], substr(x$ForeName[1], 1,1))),
                  last_author   = Author(da) %>% sapply(function(x) paste(tail(x$LastName, 1), substr(tail(x$ForeName, 1), 1,1))),
                  title   = ArticleTitle(da),
                  journal = ISOAbbreviation(da),
                  year    = YearPubmed(da),
                  abstract = AbstractText(da)),
       all_authors = Author(da) %>% sapply(function(x) paste(x$LastName, substr(x$ForeName, 1,1))) %>% unlist(),
       search_term)
}

#' Returns the number of cites for a given pubmed id (pmid)
#'
#' @param pmid the pmid
#'
#' @return a count of cites for a specific pmid
#' @export
#' @import RISmed dplyr
#'
#' @examples getCites(12345678)
getCites <- function(pmid) {
  EUtilsGet(pmid) %>%
    Cited()
}
